const path = require('path');
const fs = require('fs');

const express = require('express');
const cors = require('cors');
const upload = require('express-fileupload');
const bodyParser = require('body-parser');
const ffmpegPath = require('@ffmpeg-installer/ffmpeg').path;
const {removeBackground} = require('@imgly/background-removal-node');
const ffmpeg = require('fluent-ffmpeg');
const {generate} = require('shortid');
const axios = require('axios');

ffmpeg.setFfmpegPath(ffmpegPath);

const app = express();

app.use(bodyParser.urlencoded({extended:false}));
app.use(upload());
app.use(cors());
app.use(function (req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    res.setHeader('Access-Control-Allow-Credentials', true);
    next();});
app.use(express.static(__dirname+'/dist'));
app.use(express.static(path.join(__dirname,'public')));
app.use(express.static(path.join(__dirname,'uploads')));
app.use(express.static(path.join(__dirname,'web','public'))); // remove later
app.use(express.static(path.join(__dirname,'node_modules')));

app.post('/generate-webm',async (req,res)=>{
    var file = req.files.file;
    var png_fileName = `${generate()}.png`;
    var webm_fileName = `${generate()}.webm`;

    file.mv(`./web/public/png/${png_fileName}`,async err=>{
        if(err){
            console.log(err);
        }else{
            res.json({png_fileName,webm_fileName});
        }
    })
})

app.post('/download-usdz',(req,res)=>{
    var file = req.files.file;
    var fileName = `${generate()}.usdz`;

    file.mv(`./web/public/usdz/${fileName}`,err=>{
        if(err){
            console.log(err);
        }else{
            res.json({fileName})
        }
    })
})

app.post('/converting',(req,res)=>{
    var webm_fileName = req.body.webm_fileName;
    var png_fileName = req.body.png_fileName;

    // convertingToWebm(png_fileName,webm_fileName);
    console.log(webm_fileName,png_fileName);
})

app.post('/download-glb',(req,res)=>{
    var file = req.files.file;
    var fileName = `${generate()}.glb`;

    file.mv(`./web/public/glb/${fileName}`,err=>{
        if(err){
            console.log(err);
        }else{
            res.json({fileName})
        }
    })
})

app.post('/remove-background',(req,res)=>{
    const file = req.files.file.data;
    const fileName = `${generate()}.png`;

    removeBackground(file).then(async blob=>{
        const arrayBuffer = await blob.arrayBuffer();
        const buff = Buffer.from(arrayBuffer);
    
        fs.writeFile(`./web/public/png/${fileName}`, buff, err => { 
            if(err) throw err;
            res.json({fileName})
        });
    })
});

app.post('/download-gltf-file',(req,res)=>{
    const file = req.files.file;
    const fileName = `${generate()}.gltf`;

    file.mv(`./web/public/gltf/${fileName}`,err=>{
        if(err){
            console.log(err);
        }
        res.json({fileName:fileName});
    })
});

app.post('/download-usdz-safari',(req,res)=>{
    const file = req.files.file;
    const fileName = `${generate()}.usdz`;

    file.mv(`./web/public/usdz/${fileName}`,err=>{
        if(err){
            console.log(err);
        }
        res.json({fileName:fileName});
    })
})

app.get('/ar-safari',(req,res)=>{
    res.sendFile(
        path.join(__dirname,'public','ar-safari.html')
    );
});

app.get('/safari',(req,res)=>{
    res.sendFile(
        path.join(__dirname,'public','index-safari.html')
    );
});

app.get('/',(req,res)=>{
    res.sendFile(
        path.join(__dirname,'public','index.html')
    )
})

function convertingToWebm(png_fileName,webm_fileName){
    ffmpeg().input(`./web/public/png/${png_fileName}`)
        .loop(0.1)
        .save(path.join(__dirname,'web','public','webm',webm_fileName))
}

app.listen(8080,()=>{
    console.log('the app is listen to port 8080!');
})