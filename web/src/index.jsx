import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

const url = `https://nearable-24415.nodechef.com/ar-safari`;

const root = ReactDOM.createRoot(document.getElementById('root'));
window.onload = () => {
  var isSafari = navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0;
  
  if(isSafari){
    window.open(url,'_self');
  }
}
root.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>
);

reportWebVitals();
