import removeBackground,{Config} from '@imgly/background-removal';
import {useRef,useState,useEffect} from 'react';
import {GLTFExporter} from 'three/examples/jsm/exporters/GLTFExporter';
import {USDZExporter} from 'three/examples/jsm/exporters/USDZExporter';
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls';
import { RoomEnvironment } from 'three/examples/jsm/environments/RoomEnvironment';
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader';
import Resizer from "react-image-file-resizer";
import * as THREE from 'three';
import axios from 'axios';
import './App.css';

// const base_url = 'https://api.bychat.one';
// const base_url = 'http://localhost:8080';
const base_url = `https://nearable-24415.nodechef.com`;

let config: Config = {
  publicPath: base_url,
  progress: (key, current, total) => {
    var per = current/total;
    per*=100;
  }
};

function App() {
  const img_tag = useRef('');
  const canvas_tag = useRef('');
  const take_picture = useRef('');
  const threeContainer = useRef('');
  const percent = useRef('');
  const container2 = useRef('');
  const child = useRef('');
  const take_canvas = useRef('');
  const final_img = useRef('');
  const final_canvas = useRef('');
  const content1 = useRef('');
  const content2 = useRef('');
  const [viewWidth,setViewWidth] = useState(0);
  const [viewHeight,setViewHeight] = useState(0);
  const [loadMetaData,setLoadMetaData] = useState(false);
  const [ar_filename,setAR_filename] = useState(false);
  const [rm_png_fileName,setpng_fileName] = useState('');
  const [isIMG,setisIMG] = useState(false);
  const [USDZ_file_name,setUSDZ_file_name] = useState(false);
  const [rm_webm_fileName,setwebm_fileName] = useState('');
  const [png_new_fileName,setnew_fileName] = useState('');
 
  useEffect(()=>{
    setViewWidth(window.innerWidth);
    setViewHeight(window.innerHeight);

    navigator.mediaDevices.getUserMedia({audio:false,video:{
      width:1280,
      height:720,
      facingMode:'environment'
    }}).then(str=>{
      take_picture.current.srcObject = str;
    })
  },[])

  const resizeFile = (file,quality) =>
    new Promise((resolve) => {
      Resizer.imageFileResizer(
        file,
        1000,
        720,
        "JPEG",
        quality,
        0,
        (uri) => {
          var arr = uri.split(','),
          mime = arr[0].match(/:(.*?);/)[1],
          bstr = atob(arr[arr.length - 1]), 
          n = bstr.length, 
          u8arr = new Uint8Array(n);
          while(n--){
              u8arr[n] = bstr.charCodeAt(n);
          }
          var file = new File([u8arr], 'file.jpg', {type:mime});
          resolve(file);
        },
        "base64"
      );
  });

  const selectedFile = async e => {
    var ctxc = canvas_tag.current.getContext('2d');
    ctxc.clearRect(0, 0, canvas_tag.current.width, canvas_tag.current.height);

    const input_file = e.target;
    const file = input_file.files[0];
    const image_file = await resizeFile(file,50);

    const blob = new Blob([image_file],{type:image_file.type});
    container2.current.classList.remove('dsn');
    
    var fileSize = blob.size;
    var default_value = 0;

    var my_interval = setInterval(()=>{
      default_value+=500;
      var per = default_value/fileSize;
      per*=100;
      if(per < 100){
        child.current.style.width = per+'%';
        percent.current.innerHTML = `${Math.round(per)}% remove background`;
      }else{
        child.current.style.width ='100%';
        percent.current.innerHTML = `final check`;
      }

      if(per===100){
        percent.current.innerHTML = `final check`;
      }
    },300)

    var imageBlob = await removeBackground(blob,config);

    clearInterval(my_interval);
    child.current.style.width = 100+'%';
    percent.current.innerHTML = `final check`;

    setTimeout(()=>{
      container2.current.classList.add('dsn');
    },5000)
    
    img_tag.current.src = URL.createObjectURL(imageBlob);

    var remove_background_file = new File([imageBlob],`${Date.now()}.png`,{type:'image/png'});
    var formdata = new FormData();
    formdata.append('file',remove_background_file );

    var res = await axios.post(`${base_url}/generate-webm`,formdata);
    console.log(res.data.png_fileName,res.data.webm_fileName);
    setnew_fileName(res.data.png_fileName);
    setwebm_fileName(res.data.webm_fileName);
    setpng_fileName(res.data.png_fileName);
    setisIMG(true)


    convert_to_gltf_and_usdz(res.data.png_fileName,res.data.webm_fileName);

    setAR_filename(res.data.webm_fileName);

    new Promise(resolve=>{
      img_tag.current.onload = ()=>{
        resolve(img_tag.current);
      }
    }).then(img=>{
      const canvas = canvas_tag.current;
      canvas.width = img.width;
      canvas.height = img.height;

      var ctx = canvas.getContext('2d');
      ctx.drawImage(img,0,0);
    })
  }

  const convert_to_gltf_and_usdz = (fileName,webm_fileName_x) =>{
    var camera,scene,renderer;
  
    renderer = new THREE.WebGLRenderer({ 
        antialias: true,
        alpha:true
    });
    renderer.setPixelRatio( window.devicePixelRatio );
    renderer.setSize(window.innerWidth, window.innerHeight);
    renderer.setClearColor(0x000000, 0);
    renderer.toneMapping = THREE.ACESFilmicToneMapping;
    // document.body.appendChild(renderer.domElement);
    threeContainer.current.appendChild(renderer.domElement);
  
    camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 1000);
    camera.position.set( - 2.5, 0.6, 3.0 );
  
    const pmremGenerator = new THREE.PMREMGenerator( renderer );
  
    scene = new THREE.Scene();
    scene.background = new THREE.Color( 0xFFFFFF );
    scene.environment = pmremGenerator.fromScene( new RoomEnvironment( renderer ), 0.04 ).texture;
  
    const geometry = new THREE.BoxGeometry( 1, 1, 0.001 );
    
    const textureLoader = new THREE.TextureLoader();
    
    const png_mesh = new THREE.MeshStandardMaterial({ 
      map: textureLoader.load(`png/${fileName}`,()=>{
        const exporter = new GLTFExporter();
        exporter.parse(
          scene,
          async function(result){
            var blob = new Blob([JSON.stringify(result)],{type:'application/octet-stream'});
            var file = new File([blob],`${Date.now()}.glb`);

            var formdata = new FormData();
            formdata.append('file',file);

            var res = await axios.post(`${base_url}/download-glb`,formdata);
            var glb_file_name = res.data.fileName;
            
            const loader = new GLTFLoader().setPath("glb/");
            loader.load(glb_file_name, async function (glb) {
              const exporter = new USDZExporter();
              const arraybuffer = await exporter.parse(glb.scene);
              const blob = new Blob([arraybuffer], {
                type: "application/octet-stream"
              });

              var file = new File([blob],`${Date.now()}.usdz`);

              var formdata = new FormData();
              formdata.append('file',file);

              var res = await axios.post(`${base_url}/download-usdz`,formdata);
              var this_usdz_file_name = res.data.fileName;
              setUSDZ_file_name(this_usdz_file_name);

              var formdata = new FormData();
              formdata.append('png_fileName',fileName);
              formdata.append('webm_fileName',webm_fileName_x);

              await axios.post(`${base_url}/converting`,formdata);
            });
          },{
            binary:true
          }
        )
      }),
      transparent: true,
      needsUpdate:true,
      depthWrite:false,
      alphaWrite:true,
    });
  
    const materials = [
      png_mesh,
      png_mesh,
      png_mesh,
      png_mesh,
      png_mesh,
      png_mesh
    ];
  
    const cube = new THREE.Mesh(geometry, materials);
    scene.add(cube);
    renderer.render( scene, camera );
  
    const controls = new OrbitControls( camera, renderer.domElement );
    controls.addEventListener( 'change', ()=>{
      renderer.render( scene, camera );
    } );
  }

  const takePictureFunc = async () => {
    if(loadMetaData){
      var ctxc = canvas_tag.current.getContext('2d');
      ctxc.clearRect(0, 0, canvas_tag.current.width, canvas_tag.current.height);

      var canvas = take_canvas.current;
      var ctx = canvas.getContext('2d');
      ctx.drawImage(take_picture.current,0,0);

      canvas.toBlob(async blob=>{
        var canvas_to_file = new File([blob],'file.jpg',{type:'image/jpg'});
        
        var isSafari = navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0;
        console.log(isSafari);

        var image_file = isSafari?await resizeFile(canvas_to_file,30):await resizeFile(canvas_to_file,40);
        console.log(image_file);
        
        var main_blob = await new Promise(resolve=>{
          let reader = new FileReader();
          reader.readAsArrayBuffer(image_file);

          reader.onload = function(e) {
            var blob = new Blob([e.target.result],{type:'image/jpg'});
            resolve(blob)
          };
        });

        container2.current.classList.remove('dsn');
    
        var fileSize = main_blob.size*9.8;
        var default_value = 0;
    
        var my_interval = setInterval(()=>{
          default_value+=500;
          var per = default_value/fileSize;
          per*=100;
          if(per < 100){
            child.current.style.width = per+'%';
            percent.current.innerHTML = `${Math.round(per)}% remove background`;
          }else{
            child.current.style.width ='100%';
            percent.current.innerHTML = `final check`;
          }
    
          if(per===100){
            percent.current.innerHTML = `final check`;
          }
        },300)

        var imageBlob = await removeBackground(main_blob,config);

        clearInterval(my_interval);
        child.current.style.width = 100+'%';
        percent.current.innerHTML = `final check`;

        setTimeout(()=>{
          container2.current.classList.add('dsn');
          backToHome();
        },5000)

        var img = img_tag.current;
        img.src = URL.createObjectURL(imageBlob);

        var remove_background_file = new File([imageBlob],`${Date.now()}.png`,{type:'image/png'});
        var formdata = new FormData();
        formdata.append('file',remove_background_file );
        var res = await axios.post(`${base_url}/generate-webm`,formdata);
        setnew_fileName(res.data.png_fileName);
        setwebm_fileName(res.data.webm_fileName);
        setpng_fileName(res.data.png_fileName);
        setisIMG(true)

        convert_to_gltf_and_usdz(res.data.png_fileName,res.data.webm_fileName);
        setAR_filename(res.data.webm_fileName);

        // alert('end of convert to webm format.\n now you can see the object in AR!')

        new Promise(resolve=>{
          img_tag.current.onload = ()=>{
            resolve(img_tag.current);
          }
        }).then(img=>{
          const canvas = canvas_tag.current;
          canvas.width = img.width;
          canvas.height = img.height;
    
          var ctx = canvas.getContext('2d');
          ctx.drawImage(img,0,0);
        })
      })
    }else{
      alert('please wait!')
    }
  }

  const setDetermine = (e) => {
    setLoadMetaData(true)
    var video = e.target;
    var canvas = take_canvas.current;
    canvas.width = video.videoWidth;
    canvas.height = video.videoHeight;
  }

  const backToHome = () => {
    content1.current.classList.remove('dsn');
    content2.current.classList.add('dsn');
  }

  const openTakePictureContent = () => {
    content1.current.classList.add('dsn');
    content2.current.classList.remove('dsn');
  }

  const openArPage = () => {
    window.open(`${base_url}/?fileName=${ar_filename}?usdz_file_name=${USDZ_file_name}`,"_self");
  }

  const shareLink = async() => {
    try{
      await navigator.share({
          title: "AR",
          text: "click to see object in AR!",
          url: `${base_url}/?fileName=${ar_filename}?usdz_file_name=${USDZ_file_name}`
        });
    }catch(err){
        alert(err)
    }
  }

  return (
    <div>
      <div style={{position:'absolute'}}>
        <div style={{position:'relative'}}>
          <div style={{width:0,height:0,overflow:'scroll'}}>
            <div style={{width:'100vw',height:'100vh'}} ref={threeContainer}></div>
          </div>
          <div ref={content1} className='fcfs'>
          <div className='parent'>
            <canvas ref={canvas_tag}></canvas>
          </div><br/><br/>
          <div className='frsb wid100'>
            <div onClick={openTakePictureContent}>take picture</div>
            <div>
                <button id="share" onClick={shareLink}>Share Link</button>
            </div>
          </div>
          <form style={{marginTop:'40px'}}>
            <input 
            type='file'
            onChange={selectedFile}
            />
          </form>
          <div style={{width:0,height:0,overflow:'scroll'}}>
            <img ref={img_tag} alt='img'/>
          </div>
          {isIMG?
            <div style={{
              width:'100vw',
              height:'100vw',
              boxSizing:'border-box',
              backgroundImage:`url(png/${rm_png_fileName})`,
              backgroundSize:'100% 100%'
            }}></div>
            :
            <div style={{width:0,height:0}}></div>
          }
          </div>

        <button 
        onClick={openArPage}
        >open AR</button>

          <div className='parent'>
            <img ref={final_img} alt='img'/>
            <canvas ref={take_canvas}></canvas>
            <canvas ref={final_canvas}></canvas>
          </div>
          <div ref={content2} style={{position:'relative'}} className='dsn rrrr'>
            <video 
            autoPlay
            width={viewWidth}
            height={viewHeight}
            style={{border:'1px solid black',boxSizing:'border-box',objectFit:'cover',position:'absolute'}}
            ref={take_picture}
            onLoadedMetadata={setDetermine}
            ></video>
            <div className='frs'>
              <button 
              onClick={takePictureFunc}
              >take picture</button>
              <button 
              onClick={backToHome}
              >back</button>
              </div>
          </div>
        </div>
      </div>
      <div ref={container2} className='frc aic container2 dsn'>
        <div className='fcfs'>
          <div ref={percent} id='percent' className='frc'>50% remove background</div>
          <div className='father'>
            <div ref={child} className='child'></div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
