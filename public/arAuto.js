import * as THREE from './three.module.js';
import {ARButton} from './ARButton.js';

const VIDEO_WIDTH = 640;
const VIDEO_HEIGHT = 360;

var current_url = window.location.href;
var videoName = current_url.split('?')[1].split('=')[1];

var container;

var camera,scene,renderer;
var controller;
var video,videoCanvasContext,videoCanvas,videoTexture;

async function changeCamera(){
    navigator.mediaDevices.getUserMedia({audio:true,video:{
        facingMode:'environment'
    }})
};

const cookieObj = new URLSearchParams(document.cookie.replaceAll("&", "%26").replaceAll("; ","&")) ;

changeCamera();
init();
animate();

function init(){
    container = document.createElement('div');
    document.body.appendChild(container);

    scene = new THREE.Scene();
    camera = new THREE.PerspectiveCamera(
        70,
        window.innerWidth/window.innerHeight,
        0.01,
        200
    );

    renderer = new THREE.WebGLRenderer({antialias:true,alpha:true});
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(window.innerWidth,window.innerHeight);
    renderer.xr.enabled = true;
    container.appendChild(renderer.domElement);

    var light = new THREE.HemisphereLight(0xffffff,0xbbbbff,1);
    light.position.set(0.5,1,0.25);
    scene.add(light);

    createCanvasWithVideo();
    var videoMesh = createMeshWithVideo();
    videoMesh.position.set(0,0,-1);
    scene.add(videoMesh);

    const button = ARButton.createButton(renderer);
    document.body.appendChild(button);

    button.addEventListener('click',()=>{
        playVideo();
    });

    window.addEventListener('resize',onWindowResize,false);
}

function onWindowResize(){
    camera.aspect = window.innerWidth/window.innerHeight;
    camera.updateProjectionMatrix();

    renderer.setSize(window.innerWidth,window.innerHeight);
}

function animate(){
    renderer.setAnimationLoop(render);
}

function render(){
    if(video){
        if(video.readyState === video.HAVE_ENOUGH_DATA){
            videoCanvasContext.clearRect(0,0,VIDEO_WIDTH,VIDEO_HEIGHT);

            var vRatio = (videoCanvas.height / video.videoHeight) * video.videoWidth;

            videoCanvasContext.drawImage(video,0,0,vRatio, videoCanvas.height);
            if(videoTexture){
                videoTexture.needsUpdate = true;
            }
        }
    }document.body.setAttribute

    renderer.render(scene,camera);
}

function createCanvasWithVideo(){
    video = document.createElement('video');
    video.src = videoName;
    
    video.crossOrigin = 'anonymous';
    video.load();
    video.preload = 'auto';
    video.autoload = true;
    video.loop = true;

    videoCanvas = document.createElement('canvas');
    videoCanvas.width = VIDEO_WIDTH;
    videoCanvas.height = VIDEO_HEIGHT;

    videoCanvasContext = videoCanvas.getContext('2d');
    videoCanvasContext.fillStyle = 'rgba(255,255,255,0)';
    videoCanvasContext.fillRect(0,0,videoCanvas.width,videoCanvas.height);
}

function createMeshWithVideo(){
    const geometry = new THREE.PlaneGeometry(1.6,0.9);

    videoTexture = new THREE.Texture(videoCanvas);
    videoTexture.minFilter = THREE.LinearFilter;
    videoTexture.magFilter = THREE.LinearFilter;

    const material = new THREE.MeshLambertMaterial({
        map:videoTexture,
        side:THREE.DoubleSide,
        transparent:true
    })

    const mesh = new THREE.Mesh(geometry,material);
    return mesh;
}

function playVideo(){
    video.play();
}