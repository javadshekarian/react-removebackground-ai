import * as THREE from 'three';
import { OrbitControls } from 'OrbitControls';
import { USDZExporter } from 'USDZExporter';
import { GLTFExporter } from 'GLTFExporter';
import { RoomEnvironment } from 'RoomEnvironment.js';
import { GLTFLoader } from 'GLTFLoader.js';

const input = document.querySelector('input');
const downloadingTheFile = document.getElementById('downloadingTheFile');
const cube = document.getElementById('cube');
const imgContent = document.getElementById('imgContent');
const label = document.querySelector('label');
const plsWait = document.getElementById('plsWait');
const video = document.getElementById('showVideo');
const mainVideo = document.getElementById('mainVideo');
const canvas = document.querySelector('canvas');
const takePicMenu = document.getElementById('takePicMenu');
const takePic = document.getElementById('takePic');
const containerTwo = document.getElementById('containerTwo');
const containerThree = document.getElementById('containerThree');
const showingImg = document.getElementById('showingImg');
const move = document.getElementById('move');
const percentRemoveBackground = document.getElementById('percentRemoveBackground');
const shareLink = document.getElementById('shareLink');
const closeWindow = document.getElementById('closeWindow');

const space = '&nbsp;&nbsp;&nbsp;&nbsp;';

var png_file_name = '';
var usdz_file_name_for_open = '';
var intPercent = null;
var j = 0;

const plsWaitContent = {
  key_1:'Please wait',
  key_2:'Please wait . ',
  key_3:'Please wait . . ',
  key_4:'Please wait . . .',
}

const removeBackgroundAnim = () => {
  containerThree.classList.remove('dsn');
  intPercent = setInterval(() => {
    ++j;
    if(100<j){
      move.style.width = `100%`;
      percentRemoveBackground.innerHTML = `final check!`;
    }else{
      move.style.width = `${j}%`;
      percentRemoveBackground.innerHTML = `${j}% ${space} Remove Background`;
    }
  }, 300);
}

const endOfremoveBackgroundAnim = () => {
  percentRemoveBackground.innerHTML = `final check!`;
  move.style.width = '100%';
  clearInterval(intPercent);

  setTimeout(() => {
    intPercent = null;
    j=0;
    containerThree.classList.add('dsn');
  }, 5*1000);
}

input.onchange = () => {
    var intValal = null;
    var i = 0;
    percentRemoveBackground.innerHTML = `0% Remove Background`;
    move.style.width = `0%`;
    const file = input.files[0];

    if(file !== undefined){
      label.style.backgroundColor = 'springgreen';
      label.innerHTML = 'Selected!'
      plsWait.style.opacity = '100%';

      removeBackgroundAnim();

      intValal = setInterval(() => {
        ++i;
        plsWait.innerHTML = plsWaitContent[`key_${(i%4)+1}`];
      }, 500);
    }

    const formdata = new FormData();
    formdata.append('file',file);

    const http = new XMLHttpRequest();
    http.onloadend = e => {
      const res = JSON.parse(e.target.responseText);
      png_file_name = res.fileName;

      showingImg.style.backgroundImage = `url(png/${png_file_name})`;
      clearInterval(intValal);
      plsWait.style.opacity = '0';

      endOfremoveBackgroundAnim();

      init();
    }
    http.open('post','/remove-background');
    http.send(formdata);
}

var camera,scene,renderer;

async function init(){
  renderer = new THREE.WebGLRenderer({ 
      antialias: true,
      alpha:true
  });
  renderer.setPixelRatio( window.devicePixelRatio );
  renderer.setSize(window.innerWidth, window.innerHeight);
  renderer.setClearColor(0x000000, 0);
  renderer.toneMapping = THREE.ACESFilmicToneMapping;
  document.getElementById('cube').appendChild(renderer.domElement);

  camera = new THREE.PerspectiveCamera(20, window.innerWidth / window.innerHeight, 0.1, 1000);
  camera.position.set( - 2.5, 0.6, 3.0 );

  const pmremGenerator = new THREE.PMREMGenerator( renderer );

  scene = new THREE.Scene();
  scene.background = new THREE.Color( 0xFFFFFF );
  scene.environment = pmremGenerator.fromScene( new RoomEnvironment( renderer ), 0.04 ).texture;

  const geometry = new THREE.BoxGeometry( 1, 1, 0.001 );
  
  var materials = createMaterials();

  const cube = new THREE.Mesh(geometry, materials);
  scene.add(cube);
  render();

  const controls = new OrbitControls( camera, renderer.domElement );
  controls.addEventListener( 'change', ()=>{
    render();
  });
}

function createMaterials(){
  const textureLoader = new THREE.TextureLoader();
  
  const png_mesh = new THREE.MeshStandardMaterial({ 
    map: textureLoader.load(`./png/${png_file_name}`,()=>{
      download();
    }),
    transparent: true,
    needsUpdate:true,
    depthWrite:false,
    alphaWrite:true,
  });

  const materials = [
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh,
    png_mesh
  ];

  return materials;
}

function render() {
  renderer.render( scene, camera );
}

function download() {
  const exporter = new GLTFExporter();
  exporter.parse(
    scene,
    function(result){
      saveArrayBuffer(JSON.stringify(result),'threejsScene.glb')
    },{
      binary:true
    }
  )
}

function saveArrayBuffer(buffer,fileName){
  save(new Blob([buffer],{type:'application/octet-stream'}),fileName);
}

function save(blob){
  const file = new File([blob],'file.gltf');

  const formdata = new FormData();
  formdata.append('file',file);

  const http = new XMLHttpRequest();
  http.onloadend = e => {
    const response = JSON.parse(e.target.responseText);
    const fileName = response.fileName;

    const loader = new GLTFLoader().setPath("gltf/");
    loader.load(fileName, async function (gltf) {
      const exporter = new USDZExporter();
      const arraybuffer = await exporter.parse(gltf.scene);
      const blob = new Blob([arraybuffer], {type: "application/octet-stream"});
      const file = new File([blob],`${Date.now()}.usdz`);



      const fileName = await sendApi(file,'/download-usdz');
      usdz_file_name_for_open = fileName;
      setURL(usdz_file_name_for_open);
    });
  }
  http.open('post','/download-gltf-file');
  http.send(formdata);
}

const autoDownload = (blob) => {
  const link = document.createElement('a');
  link.download = 'file.jpg';
  link.href = URL.createObjectURL(blob);
  link.style.display ='none';
  document.body.appendChild(link);
  link.click();
}

const sendApi = (file,url) => {
  return new Promise(resolve=>{
    var formdata = new FormData();
    formdata.append('file',file);

    const http = new XMLHttpRequest();
    http.onloadend = e => {
      resolve(JSON.parse(e.target.responseText).fileName)
    }
    http.open('post',url);
    http.send(formdata);
  })
}

const setURL = (fileName) => {
  const baseUrl = window.location.href;
  const firstUrl = baseUrl.split('?')[0];
  const newUrl = `${firstUrl}?fileName=${fileName}`;
  
  history.pushState({}, null, newUrl);
  downloadingTheFile.disabled = false;
}

downloadingTheFile.addEventListener('click',()=>{
  var url = `https://nearable-24415.nodechef.com/ar-safari?fileName=${usdz_file_name_for_open}`;
  window.open(url,"_self");
})

const getPicture = {
  openCamera : async () => {
    containerTwo.classList.remove('dsn');
    const constraints = { video: { width:1280, height:720 } };
    const stream = await navigator.mediaDevices.getUserMedia(constraints);
    video.srcObject = stream;
    mainVideo.srcObject = stream;
  },
  takePictureFunc : () => {
    removeBackgroundAnim();
    getPicture.closeWindowFunction();
    const ctx = canvas.getContext('2d');
    ctx.drawImage(mainVideo,0,0,1280,720);
    var i = 0;
    var intValal;

    canvas.toBlob(async blob=>{
      const file = new File([blob],'file.jpg');

      label.style.backgroundColor = 'springgreen';
      label.innerHTML = 'Selected!'
      plsWait.style.opacity = '100%';

      intValal = setInterval(() => {
        ++i;
        plsWait.innerHTML = plsWaitContent[`key_${(i%4)+1}`];
      }, 500);

      containerTwo.classList.add('dsn');

      const fileName = await sendApi(file,'/remove-background');
      endOfremoveBackgroundAnim();
      png_file_name = fileName;

      showingImg.style.backgroundImage = `url(png/${png_file_name})`;
      clearInterval(intValal);
      plsWait.style.opacity = '0';

      init();
    })
  },
  shareLinkFunction : async () => {
    try{
      await navigator.share({
        title: "AR",
        text: "click to see object in AR!",
        url: window.location.href
      });
    }catch(err){
      console.log(err);
    }
  },
  closeWindowFunction : () => {
    containerTwo.classList.add('dsn');
  }
}

takePicMenu.addEventListener('click',()=>getPicture.openCamera());
takePic.addEventListener('click',()=>getPicture.takePictureFunc());
shareLink.addEventListener('click',()=>getPicture.shareLinkFunction());
closeWindow.addEventListener('click',()=>getPicture.closeWindowFunction());